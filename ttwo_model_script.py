import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import datasets, linear_model
from sklearn.metrics import accuracy_score, roc_auc_score, confusion_matrix
import sys
import json
import ntpath

# Define variables
lag = 30
treasury = 'treasury_rates_1_yr'
ishares = 'iShares_20+_NAV_per_share'
ishares_balance = 'iShares_20+_shares_outstanding'
us2y = 'US2Y:U.S._price'
xlk = 'xlk_close'
etf = 'etf_close'
cboe = 'cboe_spx'
ttwo = 'ttwo_close'
ttwo_s = 'ttwo_close_s%s' % lag
target = 'ttwo_binary'
target_pred = 'ttwo_binary_pred'

param_list = [treasury, ishares, us2y, xlk, etf, cboe]


def merge_shifted(_df, _ttwo, _ttwo_s, _lag):
    _df[_ttwo_s] = _df[_ttwo].shift(-_lag)
    return _df


def filter_nans(_df, _param_list, _ttwo, _ttwo_s):
    for var in _param_list + [_ttwo, _ttwo_s]:
        _df = _df[~pd.isnull(_df[var])]
    return _df


def add_target(_df, _ttwo, _ttwo_s, _target):
    _df[_target] = [int(x < y) for x, y in zip(_df[_ttwo], _df[_ttwo_s])]
    return _df


def count_nan(_df):
    return _df.isnull().values.sum()


def calc_ca(_df, _target, _target_pred):
    return len(_df.loc[_df[_target] == _df[_target_pred]]) / len(_df) * 100

if __name__ == '__main__':
    path = sys.argv[1]
    head, tail = ntpath.split(path)
    df_all = pd.read_excel(path).set_index('date')
    df = df_all
    print(len(df))
    df = merge_shifted(_df=df, _ttwo=ttwo, _ttwo_s=ttwo_s, _lag=lag)
    df = df[param_list+[ttwo, ttwo_s]]
    df = filter_nans(_df=df, _param_list=param_list, _ttwo=ttwo, _ttwo_s=ttwo_s)
    df = add_target(_df=df, _ttwo=ttwo, _ttwo_s=ttwo_s, _target=target)

    x = df[param_list]
    print(len(x))
    y = df[[target]]

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=2)

    # regr = linear_model.LinearRegression()
    regr = linear_model.LogisticRegression()
    regr.fit(x_train, y_train)

    y_test[target_pred] = regr.predict(x_test)

    y[target_pred] = regr.predict(x)
    ca = accuracy_score(y[target], y[target_pred])
    roc = roc_auc_score(y[target], y[target_pred])
    out_dict = {'Common Accuracy': ca, 'Roc Auc Score': roc}
    with open(head + '\data.json', 'w') as outfile:
        json.dump(out_dict, outfile)
    df_all.join(y[[target, target_pred]], how='left').to_excel(head + '\out_result.xlsx')

    # df_test = df_all.loc[df_all.index < '2017-01-01']
    # df_test = merge_shifted(_df=df_test, _ttwo=ttwo, _ttwo_s=ttwo_s, _lag=lag)
    # df_test = df_test[param_list+[ttwo, ttwo_s]]
    # print(count_nan(df_test))
    # df_test = filter_nans(_df=df_test, _param_list=param_list, _ttwo=ttwo, _ttwo_s=ttwo_s)
    # print(count_nan(df_test))
    # df_test = add_target(_df=df_test, _ttwo=ttwo, _ttwo_s=ttwo_s, _target=target)
    # y_test_back = df_test[[target]]
    # x_test_back = df_test[param_list]
    #
    # y_test_back[target_pred] = regr.predict(x_test_back)
    #
    # ca = calc_ca(y_test_back, target, target_pred)
    # print("Common Accuracy = %s" % ca)
    #
    # # The coefficients
    # print('Coefficients: \n', regr.coef_)
