import pandas as pd
from sklearn.metrics import accuracy_score, r2_score, confusion_matrix, roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn import datasets, linear_model
import json
import sys
import ntpath


def merge_shifted(_df, _ttwo, _ttwo_s, _lag):
    _df[_ttwo_s] = _df[_ttwo].shift(-_lag)
    return _df


def filter_nans(_df, _param_list):
    for var in _param_list:
        _df = _df[~pd.isnull(_df[var])]
    return _df


def add_binary(_df, _ttwo, _ttwo_s, _target):
    _df[_target] = [int(x < y) for x, y in zip(_df[_ttwo], _df[_ttwo_s])]
    return _df


def count_nan(_df):
    return _df.isnull().values.sum()


def calc_ca(_df, _target, _target_pred):
    return len(_df.loc[_df[_target] == _df[_target_pred]]) / len(_df) * 100

if __name__ == '__main__':

    path = sys.argv[1]
    head, tail = ntpath.split(path)
    # print(sys.argv)

    # Define variables
    lag = int(float(sys.argv[3])) if str(sys.argv[3]) != '-1' else 30
    treasury = 'treasury_rates_1_yr'
    ishares = 'iShares_20+_NAV_per_share'
    ishares_balance = 'iShares_20+_shares_outstanding'
    us2y = 'US2Y:U.S._price'
    xlk = 'xlk_close'
    etf = 'etf_close'
    cboe = 'cboe_spx'
    # ttwo = 'ttwo_close'
    ttwo = str(sys.argv[2]) if sys.argv[2] else 'ttwo_close'
    ttwo_s = ttwo + '_s%s' % lag
    # target = 'ttwo_binary'
    # target_pred = 'ttwo_binary_pred'
    target = ttwo_s
    target_pred = target + '_pred'
    target_binary_real = ttwo_s + '_grow'
    target_binary_pred = ttwo_s + '_grow_pred'
    param_list = sys.argv[4].split(',') if sys.argv[4] else [treasury, ishares, us2y, xlk, etf, cboe]
    # param_list = [treasury, ishares, us2y, xlk, etf, cboe]
    df_all = pd.read_excel(path).set_index('date')

    df_all = merge_shifted(_df=df_all, _ttwo=ttwo, _ttwo_s=ttwo_s, _lag=-lag)
    df = df_all.iloc[lag:]
    # df = df_all.loc[df_all.index <= datetime.datetime.now()-datetime.timedelta(days=lag)]
    df = df[param_list+[ttwo, ttwo_s]]
    print(df.head())
    df = filter_nans(_df=df, _param_list=param_list+[ttwo, ttwo_s])
    # df = add_target(_df=df, _ttwo=ttwo, _ttwo_s=ttwo_s, _target=target)

    x = df[param_list]
    y = df[[target]]

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=2)
    regr = linear_model.LinearRegression()
    regr.fit(x_train, y_train)

    y_test[target_pred] = regr.predict(x_test)
    y_test = y_test.join(df[ttwo], how='left')

    y_test[target_binary_real] = [int(cur_lag > cur) for cur, cur_lag in zip(y_test[ttwo], y_test[ttwo_s])]
    y_test[target_binary_pred] = [int(cur_lag > cur) for cur, cur_lag in zip(y_test[ttwo], y_test[target_pred])]
    y_test = add_binary(y_test, ttwo, ttwo_s, target_binary_real)
    y_test = add_binary(y_test, ttwo, target_pred, target_binary_pred)

    y_test.to_excel(head + "/result_test.xlsx")
    ca = accuracy_score(y_test[target_binary_real], y_test[target_binary_pred])
    roc = roc_auc_score(y_test[target_binary_real], y_test[target_binary_pred])
    tn, fp, fn, tp = confusion_matrix(y_test[target_binary_real], y_test[target_binary_pred]).ravel()
    rs = r2_score(y_test[target], y_test[target_pred])

    param_dict = dict(zip(param_list, map(lambda v: "{:.2f}".format(v), regr.coef_[0])))
    out_dict = {'Common Accuracy': "{:.2f}".format(ca), 'Roc Auc Score': "{:.2f}".format(roc),
                'True Positive': "{:.2f}".format(tp), 'False Positive':"{:.2f}".format(fp),
                'True Negative': "{:.2f}".format(tn), 'False Negative': "{:.2f}".format(fn),
                'R Squared': "{:.2f}".format(rs), 'coef': param_dict}

    with open(head + '\data.json', 'w') as outfile:
        json.dump(out_dict, outfile)

    # df_test = df_all.loc[df_all.index > datetime.datetime.now()-datetime.timedelta(days=lag)]

    # Real part - last 30 days prediction
    df_test = df_all.iloc[:lag]

    df_test = df_test[param_list+[ttwo]]
    df_test = filter_nans(_df=df_test, _param_list=param_list+[ttwo])

    df_test[target_pred] = regr.predict(df_test[param_list])
    df_test = add_binary(df_test, ttwo, target_pred, target_binary_pred)
    df_test.to_excel(head + "/result_real.xlsx")
