package com.filehelper;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;
import com.controller.MainController;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;

public class FileHelperController {

    @FXML
    Button btnChooseAnotherFile;

    @FXML
    ImageView imageFileChooserType;
    @FXML
    Label labelFileChooserPath;
    @FXML
    TableView<ObservableList<StringProperty>> tableFileChooser;
    @FXML
    ChoiceBox choiceFileChooser;

    private File file;
    private MainController basicController;
    private String path;
    private int typeFile;

    private ArrayList<String> colums;

    public void setBasicController(MainController controller) {
        this.basicController = controller;
    }

    public void setFile(File file) {
        this.file = file;

        path = file.getAbsolutePath();
        labelFileChooserPath.setText(path);
        if (path.endsWith(".xls") || path.endsWith(".xlsx")) {
//            Image image = new Image("C:\\Users\\balya\\IdeaProjects\\predictive_model\\src\\main\\resources\\images\\excel.png");
//            imageFileChooserType.setImage(image);
            typeFile = 1;
        } else if (path.endsWith(".json")) {
//            Image image = new Image("/main/resources/images/JSON.png");
//            imageFileChooserType.setImage(image);
            typeFile = 2;
        } else if (path.endsWith(".csv")) {
//            Image image = new Image("/main/resources/images/csv.png");
//            imageFileChooserType.setImage(image);
            typeFile = 3;
        }
        loadDataToFile(path);
    }

    public void confirm(ActionEvent actionEvent) {
        basicController.setPathFile(path);
        basicController.setFile(file);
        try {
            basicController.setDataColumns(colums);
            basicController.setTargetValue(choiceFileChooser.getValue().toString());
        } catch (NullPointerException e) {
            basicController.setTargetValue("");
        }

        Stage stage = (Stage) btnChooseAnotherFile.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void chooseAnother(ActionEvent actionEvent) {
        if (null != basicController) {
            Stage stage = (Stage) btnChooseAnotherFile.getScene().getWindow();
            stage.close();
            basicController.chooseFile(new ActionEvent());
        }
    }

    private void loadDataToFile(String url) {
        tableFileChooser.getItems().clear();
        tableFileChooser.getColumns().clear();
        tableFileChooser.setPlaceholder(new Label("Loading..."));
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    FileReader fr = new FileReader(url);
                    BufferedReader in = new BufferedReader(fr);
                    String headerLine = in.readLine();
                    if (typeFile == 1) {
                        XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(file));
                        XSSFSheet sheet = workbook.getSheetAt(0);
                        for (int i = 0; i < 1; i++) {
                            StringBuilder data = new StringBuilder();
                            XSSFRow row = sheet.getRow(i);
                            for (int j = 0; j < row.getLastCellNum(); j++) {
                                try {
                                    data.append(row.getCell(j).toString());
                                } catch (NullPointerException e) {
                                    data.append("");
                                }
                                data.append(";");
                            }
//                            System.out.println(data);
                            headerLine = data.toString();
                        }
                    } else if (typeFile == 2) {

                    } else if (typeFile == 3) {

                    }
                    //System.exit(0);

                    String[] dataValues;
                    dataValues = headerLine.split(";");
                    if (dataValues.length < 5) {
                        dataValues = headerLine.split(",");
                    }
                    if (dataValues.length < 5) {
                        dataValues = headerLine.split("\t");
                    }

                    final String[] dValue = dataValues;

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            for (int column = 0; column < dValue.length; column++) {
                                tableFileChooser.getColumns().add(
                                        createColumn(column, dValue[column]));
                            }
                        }
                    });

                    ArrayList<String> arr = new ArrayList<>();
                    arr.addAll(Arrays.asList(dValue));
                    choiceFileChooser.getItems().setAll(dValue);
                    colums = new ArrayList<>();
                    colums.addAll(arr);

                    // Data:

                    String dataLine;
                    int k = 1;
                    if (typeFile == 1) {
                        XSSFWorkbook workbook2 = new XSSFWorkbook(new FileInputStream(file));
                        XSSFSheet sheet2 = workbook2.getSheetAt(0);
                        StringBuilder data2 = new StringBuilder();
                        XSSFRow row;
                        while ((dataLine = in.readLine()) != null) {
//                            System.out.println(k);
                            if (typeFile == 1) {
                                row = sheet2.getRow(k++);
                                if (null != row)
                                    for (int j = 0; j < row.getLastCellNum(); j++) {
                                        try {
                                            data2.append(row.getCell(j).toString());
                                        } catch (NullPointerException e) {
                                            data2.append("");
                                        }
                                        data2.append(";");
                                    }
                                dataLine = data2.toString();
                                data2.setLength(0);
                            }
                            String[] dataValues2;
                            dataValues2 = dataLine.split(";");
                            if (dataValues.length < 5) {
                                dataValues2 = dataLine.split(",");
                            }
                            if (dataValues.length < 5) {
                                dataValues2 = dataLine.split("\t");
                            }
                            final String[] dValue2 = dataValues2;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    // Add additional columns if necessary:
                                    for (int columnIndex = tableFileChooser.getColumns().size(); columnIndex < dValue2.length; columnIndex++) {
                                        tableFileChooser.getColumns().add(createColumn(columnIndex, ""));
                                    }
                                    // Add data to table:
                                    ObservableList<StringProperty> data = FXCollections
                                            .observableArrayList();
                                    for (String value : dValue2) {
                                        data.add(new SimpleStringProperty(value));
                                    }
                                    tableFileChooser.getItems().add(data);
                                }
                            });
                        }
                    } else if (typeFile == 3) {
                        while ((dataLine = in.readLine()) != null) {
                            String[] dataValues2;
                            dataValues2 = dataLine.split(";");
                            if (dataValues.length < 5) {
                                dataValues2 = dataLine.split(",");
                            }
                            if (dataValues.length < 5) {
                                dataValues2 = dataLine.split("\t");
                            }

                            final String[] dValue2 = dataValues2;

                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    // Add additional columns if necessary:
                                    for (int columnIndex = tableFileChooser.getColumns().size(); columnIndex < dValue2.length; columnIndex++) {
                                        tableFileChooser.getColumns().add(createColumn(columnIndex, ""));
                                    }
                                    // Add data to table:
                                    ObservableList<StringProperty> data = FXCollections
                                            .observableArrayList();
                                    for (String value : dValue2) {
                                        data.add(new SimpleStringProperty(value));
                                    }
                                    tableFileChooser.getItems().add(data);
                                }
                            });
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };


        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.run();


    }


    private TableColumn<ObservableList<StringProperty>, String> createColumn(
            final int columnIndex, String columnTitle) {
        TableColumn<ObservableList<StringProperty>, String> column = new TableColumn<>();
        String title;
        if (columnTitle == null || columnTitle.trim().length() == 0) {
            title = "Column " + (columnIndex + 1);
        } else {
            title = columnTitle;
        }
        column.setText(title);
        column
                .setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList<StringProperty>, String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(
                            TableColumn.CellDataFeatures<ObservableList<StringProperty>, String> cellDataFeatures) {
                        ObservableList<StringProperty> values = cellDataFeatures.getValue();
                        if (columnIndex >= values.size()) {
                            return new SimpleStringProperty("");
                        } else {
                            return cellDataFeatures.getValue().get(columnIndex);
                        }
                    }
                });
        return column;
    }

}
