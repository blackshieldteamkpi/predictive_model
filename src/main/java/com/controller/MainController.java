package com.controller;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.filehelper.FileHelperController;
import netscape.javascript.JSObject;
import org.json.JSONObject;
import org.json.JSONStringer;
import sun.rmi.runtime.Log;

import javax.swing.text.html.ImageView;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainController {

    @FXML
    Label filePath;
    @FXML
    TextArea resultArea;
    @FXML
    TextField pythonTextField;

    @FXML
    ComboBox lagBox;
    @FXML
    ComboBox targetBox;
    @FXML
    ComboBox addBox;
    @FXML
    ComboBox removeBox;
    @FXML
    TextArea regressorField;

    ObservableList<String> options = FXCollections.observableArrayList("0", "5", "10", "20", "30", "60", "120", "180");


    private File file;
    private String pathFile;
    private String targetValue;
    private String pythonPathData;

    private ArrayList<String> availableRegressors = new ArrayList<>();
    private ArrayList<String> realRegressors = new ArrayList<>();


    private ArrayList<String> dataColumns;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String path) {
        this.pathFile = path;
        if (path.length() > 0)
            filePath.setText("Файл: " + path);
    }

    public String getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
        targetBox.getSelectionModel().select(targetValue);
    }

    public ArrayList<String> getDataColumns() {
        return dataColumns;
    }

    public void setDataColumns(ArrayList<String> dataColumns) {
        this.dataColumns = dataColumns;
        availableRegressors = dataColumns;
        ObservableList<String> c = new ObservableListWrapper<String>(dataColumns);
        addBox.setItems(c);
        targetBox.setItems(c);
    }

    @FXML
    public void initialize() {
        try (BufferedReader br = new BufferedReader(new FileReader("python_path.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            pythonPathData = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        pythonTextField.setText(pythonPathData);

        lagBox.setItems(options);
        lagBox.getSelectionModel().select(3);
    }

    public void chooseFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel & JSON files (*.xls, *.xlsx, *.csv, *.json)",
                "*.xls", "*.xlsx", "*.csv", "*.json");
        fileChooser.getExtensionFilters().addAll(extFilter);
//        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")+"/Dropbox/Work/predictive_model"));

        //Show open file dialog

        file = fileChooser.showOpenDialog(null);
        if (null != file) { //если пользователь выбрал какой-то файл, то продолжаем работу с ним
            try {
                Stage stage = new Stage();
                String fxmlFile = "/fxml/fileChooser.fxml";
                FXMLLoader loader = new FXMLLoader();
                Parent root = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));
                stage.setTitle("Выберите файл");
                stage.setScene(new Scene(root));
                stage.show();

                FileHelperController controller = loader.getController();
                controller.setFile(file);
                controller.setBasicController(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private void writeToFile(String strPath) {
        RunnableWriting write = new RunnableWriting();
        write.setStr(strPath);
        write.run();
    }

    public void startScript(ActionEvent actionEvent) {
        String path = System.getProperty("user.dir");

        String pythonPath = pythonTextField.getText();
        if (!pythonPath.isEmpty()) {

            writeToFile(pythonPath);

            try {
                ArrayList<String> notNullArray = new ArrayList<>();
                for (String item : realRegressors)
                    if (null != item)
                        notNullArray.add(item);

                StringBuilder r = new StringBuilder();
                for (int i = 0; i < notNullArray.size(); i++) {
                    if (i != notNullArray.size() - 1)
                        r.append(notNullArray.get(i)).append(",");
                    else r.append(notNullArray.get(i));

                }

                System.out.println("Start script");
                String runPath = pythonPath + " " + path + "\\ttwo_model_cont_script.py" + " " + pathFile + " " + targetValue + " " + lagBox.getValue() + " " + r;
//                String runPath = pythonPath + " " + path + "\\ttwo_model_cont_script.py" + " " + pathFile + " ttwo_close 20 treasury_rates_1_yr,iShares_20+_NAV_per_share,xlk_close";
                System.out.println("Path = " + runPath);
                Process process = Runtime.getRuntime().exec(runPath);
                process.waitFor();

                showResult();
                System.out.println("End script");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void showResult() {
        try {


            int lastPosition = pathFile.lastIndexOf("\\");
            String newURL = pathFile.substring(0, lastPosition + 1);
            String newPath = newURL + "data.json"; //Name of file which generated in python script

//            System.out.println("Path = " + newPath);

            BufferedReader reader = new BufferedReader(new FileReader(newPath));
            String json = "";

            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = reader.readLine();
            }
            json = sb.toString();

            JSONObject object = new JSONObject(json);
            Double accuracy = object.getDouble("Common Accuracy");
            Double rScore = object.getDouble("Roc Auc Score");
            Double tPos = object.getDouble("True Positive");
            Double fPos = object.getDouble("False Positive");
            Double tNeg = object.getDouble("True Negative");
            Double fNeg = object.getDouble("False Negative");
            Double rSquare = object.getDouble("R Squared");

            ArrayList <Double> values = new ArrayList<>();

            ArrayList<String> notNullArray = new ArrayList<>();
            for (String item : realRegressors)
                if (null != item)
                    notNullArray.add(item);

            for (String str: notNullArray) {
                Double d = object.getJSONObject("coef").getDouble(str);
                values.add(d);
            }

            StringBuilder coefResult = new StringBuilder();
            for (int i = 0; i < values.size(); i++) {
                String s = notNullArray.get(i) + " = " + values.get(i) + " ";
                coefResult.append(s);
            }

            resultArea.appendText(String.format("Common Accuracy = %.5f\nRoc Auc Score = %.5f\nTrue Positive = %.2f\nFalse Positive = %.2f\nTrue Negative = %.2f\nFalse Negative = %.2f\nR Square = %.2f\nCoefficients :\n" + coefResult + "\n---------------------------------\n", accuracy, rScore, tPos, fPos, tNeg, fNeg, rSquare));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addRegressor(ActionEvent actionEvent) {
        try {
            if (!realRegressors.contains(addBox.getValue())) {
                realRegressors.add((String) addBox.getValue());
                availableRegressors.remove(addBox.getValue());
                StringBuilder text = new StringBuilder();
                for (String str : realRegressors) {
                    if (null != str)
                        text.append(str).append(" ");
                }
                regressorField.setText(text.toString());
                ObservableList<String> available = new ObservableListWrapper<String>(availableRegressors);
                addBox.setItems(available);

                ObservableList<String> real = new ObservableListWrapper<String>(realRegressors);
                removeBox.setItems(real);
            }
        } catch (Exception e) {

        }
    }

    public void removeRegressor(ActionEvent actionEvent) {
        try {
            if (!availableRegressors.contains(removeBox.getValue())) {
                availableRegressors.add((String) removeBox.getValue());
                realRegressors.remove(removeBox.getValue());
                StringBuilder text = new StringBuilder();
                for (String str : realRegressors) {
                    text.append(str).append(" ");
                }
                regressorField.setText(text.toString());

                ObservableList<String> available = new ObservableListWrapper<String>(availableRegressors);
                addBox.setItems(available);

                ObservableList<String> real = new ObservableListWrapper<String>(realRegressors);
                removeBox.setItems(real);
            }
        } catch (Exception e) {

        }
    }


    class RunnableWriting implements Runnable {

        String str;

        public String getStr() {
            return str;
        }

        public void setStr(String str) {
            this.str = str;
        }

        @Override
        public void run() {
            Path file = Paths.get("python_path.txt");
            List<String> lines = Collections.singletonList(str);
            try {
                Files.write(file, lines, Charset.forName("UTF-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
